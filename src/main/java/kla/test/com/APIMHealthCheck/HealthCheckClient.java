package kla.test.com.APIMHealthCheck;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class HealthCheckClient {
	
	

	public static void healthCheckImpl(String host, String port, int retryCount) {
		
		int maxRetryCount = 4;
		int waitSecondsBetweenRetries=3;
		String errorCode,errorMessage;
		
		try {
			
			String urlString = "http://"+host+":"+port+"/services/Version";
			URL url = new URL(urlString);
			
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");

			 
			if(conn.getResponseCode() == 200) {
				System.out.println("Server heart beat is running");
			}
			
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
			

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}

			conn.disconnect();
		}
		catch(MalformedURLException e) {
			System.out.println("Url is not correct");
			errorCode="ERR_01";
			errorMessage="Url is not correct";
			SendEmailNotification.sendEmail(errorCode, errorMessage, "");
			
		}
		catch(IOException e) {
			System.out.println("Server refused to connect");
			
			if(retryCount == maxRetryCount) {
				errorCode="ERR_02";
				errorMessage="Server refused to connect";
				String errorDescription = "Server refused to connect:"+retryCount + "consecutive runs have failed";
				SendEmailNotification.sendEmail(errorCode, errorMessage, errorDescription);
			}
			else {
				retryCount++;
				try {
					TimeUnit.SECONDS.sleep(waitSecondsBetweenRetries);
				} catch (InterruptedException e1) {
				}
				healthCheckImpl(host,port,retryCount);
			}
			
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
